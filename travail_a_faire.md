# Projet de Site eCommerce

## Contexte du Projet
Le projet consiste à tester, build, et déployer un site eCommerce avec un backend développé en Spring Boot et un frontend avec Angular. La base de données utilisée est MySQL.

## Technologies Utilisées
- **Angular:** 15.0.0
- **Spring Boot:** 3.0.1
- **Angular CLI:** 15.0.0
- **Node.js:** 18.12.1
- **SASS:** 1.56.0

## Variables d'Environnement
Pour la base de données MySQL, les variables d'environnement suivantes sont utilisées :
- `USERNAME`
- `PASSWORD`
- `DB_NAME`

## Gestion des secrets
Utilisez le service secret AWS pour gérer les secrets de l'application 
exemple : creds de docker hub.

## Commandes pour tests
### Backend :
``cd ~/backend``

``mvn test``

### Frontend :
``cd ~/frontend``

``npm install``

``npm run test``

## Commandes pour build
### Backend :
``cd ~/backend``

``mvn package``

### Frontend :
``cd ~/frontend``

``npm install``

``npm run build:ssr``

## Images de Base pour les Dockerfiles
1. backend : `eclipse-temurin:17-jdk-alpine`
2. fronted: `node:18-alpine`
3. mysql : `mysql:8`

## Étapes à Suivre pour le TP:
1. Récupérer le projet `https://gitlab.com/massinfoo/tp_devops` sur GitLab en effectuant un `FORK` du projet.
2. Créer trois branches : `dev`, `stg`, et `prd`. 

**remarque**: Dans un premier temps, vous travaillerez uniquement sur la branche `dev` et l'environnement de dev.
3. Compléter les Dockerfiles pour le backend, le frontend et MySQL suivants :

### Frontend Dockerfile
```dockerfile
...
COPY default.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
...

EXPOSE 4000
CMD ["nginx", "-g", "daemon off;"]
```

### Backend Dockerfile
```dockerfile
...
VOLUME /tmp
...
ENTRYPOINT ["java","-jar","*******.jar"]
```
### Mysql Dockerfile
```dockerfile
image: mysql:8
...
EXPOSE 3306
....
```

4. Configurer le runner GitLab pour exécuter les jobs de la CI/CD (utilisez EC2).

5. Créer une CI pour l'installation des dépendances de build, les tests unitaires, la construction des binaires, la construction des images Docker, et le push sur Docker Hub pour le backend, le frontend, et MySQL.

6. Créer une CD pour déployer toute l'application sur une instance EC2 AWS dans des conteneurs Docker.
